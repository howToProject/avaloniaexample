using Avalonia.Controls;

namespace AvaloniaExample;

public partial class MainWindow : Window
{
    public MainWindow()
    {
        InitializeComponent();
        DataContext = this;
    }

    public string BoundString { get; } = "(String from MainWindow.axaml.cs)";
}