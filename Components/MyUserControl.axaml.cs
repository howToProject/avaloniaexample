using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace AvaloniaExample.Components;

public partial class MyUserControl : UserControl
{
    public required string StringOnMyUserControl { get; set; }
    // required is ignored by axaml
    
    // this seems to be the easiest way to work with reactive propoerties
    #region ReactiveStringOnMyUserControl
    public static readonly StyledProperty<string> ReactiveStringOnMyUserControlProperty =
        AvaloniaProperty.Register<MyUserControl, string>(nameof(ReactiveStringOnMyUserControl));

    public string ReactiveStringOnMyUserControl
    {
        get => GetValue(ReactiveStringOnMyUserControlProperty);
        set => SetValue(ReactiveStringOnMyUserControlProperty, value);
    }
    #endregion
    
    public MyUserControl()
    {
        DataContext = this;
        InitializeComponent();
    }
}