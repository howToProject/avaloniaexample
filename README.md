# AvaloniaExample

## Goal

Example project for avalonia. This is supposed getting started with the technology and help remembering 
the most important concepts and syntax.

## Create a project

    dotnet list avalonia
    dotnet new avalonia.app --name AvaloniaExample

## More about Avalonia

https://avaloniaui.net/

https://docs.avaloniaui.net/

https://reference.avaloniaui.net/api/

https://github.com/AvaloniaUI/Avalonia/tree/master/src

Seemingly the only youtube-tutorial (May 2024):

 * https://www.youtube.com/watch?v=YhKuZImznEE&list=PLrW43fNmjaQWMhFHxS1jpQ34TkHroHJLb Playlist
 * https://www.youtube.com/watch?v=XmYKOoGlZWo TemplatedControls


## Topics include

 * Basic building blocks
 * Bind properties
   * Define axaml namespaces (e.g. `xmlns:components="clr-namespace:AvaloniaExample.Components"`)
   * Define DataType in axaml (e.g. `x:DataType="avaloniaExample:MainWindow"`)
   * DataContext in code behind (e.g. `DataContext = this`)
   * Syntax (e.g. `<Run Text="{Binding BoundString}" />`)
 * Create reactive properties in code behind
   
        #region ReactiveStringOnMyUserControl
        public static readonly StyledProperty<string> ReactiveStringOnMyUserControlProperty =
        AvaloniaProperty.Register<MyUserControl, string>(nameof(ReactiveStringOnMyUserControl));
        
        public string ReactiveStringOnMyUserControl
        {
        get => GetValue(ReactiveStringOnMyUserControlProperty);
        set => SetValue(ReactiveStringOnMyUserControlProperty, value);
        }
        #endregion

[//]: # ( * )


## Todo

 * Events
 * TemplatedUserControl
 * Styling
